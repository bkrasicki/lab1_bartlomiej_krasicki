﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab1_Krasicki.Rest.Context;
using Lab1_Krasicki.Rest.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Lab1_Krasicki.Rest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private AzureDbContext _context;

        public PeopleController()
        {
            _context = new AzureDbContext();
        }

        [HttpGet]
        public ActionResult<IEnumerable<Person>> GetPersons()
        {
            var people = _context.People.ToList();
            if (people == null)
                return NoContent();
            return people;
        }

        [HttpGet("{id}")]
        public ActionResult<Person> GetPerson(int id)
        {
            var person = _context.People.SingleOrDefault(p => p.PersonId == id);
            if (person == null)
                return NotFound();
            return person;
        }

        [HttpPost]
        public ActionResult AddPerson([FromBody] Person person)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            _context.People.Add(person);
            _context.SaveChanges();

            return Ok();
        }

        [HttpPut("{id}")]
        public ActionResult UpdatePerson(int id, [FromBody] Person person)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var personInDb = _context.People.SingleOrDefault(p => p.PersonId == id);

            personInDb.FirstName = person.FirstName;
            personInDb.LastName = person.LastName;

            _context.SaveChanges();

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult DeletePerson(int id)
        {
            var personToDelete = _context.People.SingleOrDefault(p => p.PersonId == id);

            if (personToDelete == null)
                return NotFound();

            _context.People.Remove(personToDelete);
            _context.SaveChanges();

            return Ok();
        }
    }
}